package com.juniorisep.project.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.juniorisep.person.entities.Member;
import com.juniorisep.project.enums.ProjectStatus;
import com.juniorisep.project.enums.Type;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GraphQLQuery(name = "id", description = "a Project's unique id")
    private Long id;
    @GraphQLQuery
    private String name;
    @GraphQLQuery
    private String description;
    @Enumerated(EnumType.STRING)
    @GraphQLQuery(description = "Project's type or concerned unit")
    private Type type;
    @Enumerated(EnumType.STRING)
    @GraphQLQuery(description = "Status of the currentProject")
    private ProjectStatus status = ProjectStatus.TO_START;

    @OneToMany(mappedBy = "project")
    @GraphQLQuery
    private List<Task> tasks;

    @GraphQLQuery(name="tag", description = "optional tag for the Project. Ex: Phoenix, Norme ISO, Audit")
    private String tag;

    @ManyToMany
    @GraphQLQuery(description = "Members Participating in this project")
    private List<Member> participants;

    @GraphQLQuery
    private LocalDateTime createdAt = LocalDateTime.now();
}
