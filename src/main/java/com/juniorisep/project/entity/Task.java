package com.juniorisep.project.entity;

import io.leangen.graphql.annotations.GraphQLId;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Table(name = "project_tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GraphQLQuery(name = "id", description = "a Task's unique id")
    private Long id;
    @GraphQLQuery
    private String name;
    @GraphQLQuery
    private String description;
    @GraphQLQuery(description = "wen a task was created")
    private LocalDateTime createdAt = LocalDateTime.now();
    @GraphQLQuery
    private LocalDateTime startedAt;
    @GraphQLQuery
    private LocalDateTime finishedAt;
    @ManyToOne
    @GraphQLQuery
    private Project project;
}
