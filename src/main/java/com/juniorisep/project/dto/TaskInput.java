package com.juniorisep.project.dto;

import com.juniorisep.project.entity.Task;
import io.leangen.graphql.annotations.GraphQLId;
import io.leangen.graphql.annotations.GraphQLInputField;
import io.leangen.graphql.annotations.GraphQLNonNull;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


/**
 *  This class is an example of DTO to Avoid having to pass each attribute in the controller's arguments
 *  while creating or updating an Entity (therefore using the ModelMapper to persist changes).
 */
@Getter
@Setter
@NoArgsConstructor
public class TaskInput extends Task {
    @GraphQLNonNull
    @GraphQLId
    private Long id;
    @GraphQLQuery(description = "The Project Job's Id")
    private Long projectId;

}
