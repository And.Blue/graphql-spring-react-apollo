package com.juniorisep.project.repository;

import com.juniorisep.person.entities.Member;
import com.juniorisep.project.entity.Project;
import com.juniorisep.project.entity.Task;
import com.juniorisep.project.enums.ProjectStatus;
import com.juniorisep.project.enums.TaskStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
    List<Project> findAll();
    List<Project> findByStatus(ProjectStatus status);
}
