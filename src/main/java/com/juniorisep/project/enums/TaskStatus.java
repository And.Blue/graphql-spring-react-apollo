package com.juniorisep.project.enums;

public enum TaskStatus {
    TO_START, WIP, DONE, CANCELLED
}
