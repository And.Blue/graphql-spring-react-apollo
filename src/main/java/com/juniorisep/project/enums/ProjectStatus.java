package com.juniorisep.project.enums;

public enum ProjectStatus {
    TO_START, PLANNING, WIP, DONE, CANCELLED
}
