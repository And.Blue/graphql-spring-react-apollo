package com.juniorisep.project.controller;

import com.juniorisep.project.dto.TaskInput;
import com.juniorisep.project.enums.TaskStatus;
import com.juniorisep.project.repository.ProjectRepository;
import com.juniorisep.project.repository.TaskRepository;
import io.leangen.graphql.annotations.*;
import com.juniorisep.project.entity.Task;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@GraphQLApi
@Service
public class TaskController {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final ModelMapper mapper;
    //for Web Sockets
//    private final ConcurrentMultiMap<String, FluxSink<Task>> subscribers = new ConcurrentMultiMap<>();

    @Autowired
    public TaskController(TaskRepository taskRepository, ProjectRepository projectRepository, ModelMapper mapper) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.mapper = mapper;
    }


    @GraphQLMutation(description = "Creates a task and adds it to the Project.")
    public Task addTask(
            @GraphQLNonNull @GraphQLArgument(name = "projectId", description = "careful, add an existing project id") Long projectId,
            @GraphQLNonNull @GraphQLArgument(name = "name", description = "New Task's name") String name,
             String description) {
        Task newTask = new Task();
        newTask.setProject(projectRepository.findById(projectId).orElseThrow(EntityNotFoundException::new));
        if(!description.isEmpty())
            newTask.setDescription(description);
        newTask.setName(name);
        return taskRepository.save(newTask);
    }

    @GraphQLMutation
    public Task updateTask(@GraphQLNonNull TaskInput task) {
        Task taskToUpdate = taskRepository.findById(task.getId()).orElseThrow(EntityNotFoundException::new);
        mapper.map(task, taskToUpdate);
        //Notify all the subscribers following this task
        //subscribers.get(code).forEach(subscriber -> subscriber.next(task));
        return taskRepository.save(task);
    }

    @GraphQLQuery
    public List<Task> tasks() {
        return taskRepository.findAll();
    }

    @GraphQLQuery(description = "Computed value for Task that determines the status based on dates.")
    public TaskStatus status(@GraphQLContext Task task) {
        if(task.getFinishedAt() != null)
            return TaskStatus.DONE;
        if(task.getStartedAt() != null)
            return TaskStatus.WIP;
        if(task.getCreatedAt() != null)
            return TaskStatus.WIP;
        return TaskStatus.CANCELLED;
    }

//    @GraphQLSubscription
//    public Publisher<Task> taskStatusChanged(String code) {
//        return Flux.create(subscriber -> subscribers.add(code, subscriber.onDispose(() -> subscribers.remove(code, subscriber))), FluxSink.OverflowStrategy.LATEST);
//    }
}
