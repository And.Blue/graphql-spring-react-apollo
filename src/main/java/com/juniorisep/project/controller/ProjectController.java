package com.juniorisep.project.controller;

import com.juniorisep.person.entities.Member;
import com.juniorisep.person.repository.MemberRepository;
import com.juniorisep.project.enums.Type;
import com.juniorisep.project.repository.ProjectRepository;
import io.leangen.graphql.annotations.*;
import com.juniorisep.project.entity.Project;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@GraphQLApi
@Service
public class ProjectController {

    private final ProjectRepository projectRepository;
    private final MemberRepository memberRepository;

    @Autowired
    public ProjectController(ProjectRepository projectRepository, MemberRepository memberRepository) {
        this.projectRepository = projectRepository;
        this.memberRepository = memberRepository;
    }

    @GraphQLQuery
    public Project project(Long id) {
        return projectRepository.findById(id).orElseThrow(EntityExistsException::new);
    }

    @GraphQLMutation
    public Project createProject(@GraphQLNonNull String name, String description,
             @GraphQLArgument(name = "tags", description = "Optional Tags that help define the Project", defaultValue = "") String tag,
             @GraphQLArgument(name = "type", description = "The project's type/unit") Type type
            ) {
        Project newProject = new Project();
        newProject.setName(name);
        newProject.setDescription(description);
        if (type == null)
            type = Type.AUTRE;
        newProject.setType(type);
        newProject.setTag(tag);
        return projectRepository.save(newProject);
    }

    @GraphQLQuery
    public List<Project> projects() {
        return projectRepository.findAll();
    }

    @GraphQLQuery(name = "isTech")
    public boolean isTech(@GraphQLContext Project project) {
        return project.getType() == Type.TECH;
    }

    @GraphQLMutation(description = "adds one or several members to a project using Ids.")
    public Project updateParticipants(
            @GraphQLNonNull Long id,
            @GraphQLNonNull
            @GraphQLArgument(description = "update list of participants (no add or remove)", name = "participantIds")
                    List<Long> ids) {

        Project projectToUpdate = projectRepository.findById(id).orElseThrow(EntityExistsException::new);
        List<Member> updatedParticipants = memberRepository.findByIdIn(ids);
        projectToUpdate.setParticipants(updatedParticipants);

        return projectRepository.save(projectToUpdate);
    }

    @GraphQLQuery
    public long currentFunding(@GraphQLContext Project project) {
        return ThreadLocalRandom.current().nextInt(1000) * 1000;
    }
}
