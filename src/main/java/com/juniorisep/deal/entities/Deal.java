package com.juniorisep.deal.entities;

import com.juniorisep.deal.enums.OrderStatus;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Table(name = "deals")
public class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GraphQLQuery(description = "a Deals's unique id")
    private Long id;
    @GraphQLQuery(description = "Fullname of the client")
    private String clientName;
    @GraphQLQuery(description = "Alias name of the dealer")
    private String dealerName;
    @GraphQLQuery(description = "Location of the Deal")
    private String location;


    @GraphQLQuery(description = "Status of the order")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @ManyToMany(mappedBy = "orders",fetch = FetchType.LAZY)
    @GraphQLQuery(description = "Products sold")
    private List<Product> merchandise;


}
