package com.juniorisep.deal.entities;
import com.juniorisep.deal.enums.ProductType;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.*;
import java.util.List;


@Data
@EqualsAndHashCode
@Getter
@Entity
@Setter
@NoArgsConstructor
@ToString
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GraphQLQuery(description = "a Product's unique id")
    private Long id;
    @GraphQLQuery(description = "Product name")
    private String name;
    @GraphQLQuery(description = "Price of a the packaged product (total weight)")
    private float price;
    @GraphQLQuery(description = "Weight/quantity of the product in grams/ml")
    private float quantity;
    @Enumerated(EnumType.STRING)
    @GraphQLQuery(description = "Type of the product, used to identity what it is and the weigth/quantity of it")
    private ProductType type;
    @GraphQLQuery(description = "Orders containing this product")
    @ManyToMany
    private List<Deal> orders;
    @GraphQLQuery
    @Column(columnDefinition="text")
    private String description;
}
