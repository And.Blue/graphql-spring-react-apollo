package com.juniorisep.deal.enums;

public enum OrderStatus {
    DELIVERED,
    FAILED,
    IN_PROGRESS,
    POLICE_ALERT
}
