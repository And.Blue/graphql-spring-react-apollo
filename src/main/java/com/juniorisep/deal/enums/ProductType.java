package com.juniorisep.deal.enums;

public enum ProductType {
    LIQUID,
    POWDER,
    PLANT,
    GUMMY
}
