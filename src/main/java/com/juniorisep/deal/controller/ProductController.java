package com.juniorisep.deal.controller;


import com.juniorisep.deal.dtos.NewProduct;
import com.juniorisep.deal.entities.Product;
import com.juniorisep.deal.repository.ProductRepository;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@GraphQLApi
public class ProductController {
    private final ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @GraphQLQuery(name = "products", description = "retrieves every member of databae")
    public List<Product> getAllProducts() {
        return this.productRepository.findAll();
    }

    @GraphQLQuery(name = "productById", description = "gets a member by his id")
    public Product getProduct(long id) {
        return this.productRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @GraphQLMutation(name = "saveProduct", description = "Saves a person to DB")
    public Product saveProdut(@GraphQLArgument(name = "NewProduct", description = "Product to add") NewProduct productToSave) {
        Product newProduct = new Product();
        //TODO : Use ModelMapper
        newProduct.setName(productToSave.getName());
        newProduct.setDescription(productToSave.getDescription());
        newProduct.setPrice(productToSave.getPrice());
        newProduct.setQuantity(productToSave.getQuantity());
        newProduct.setType(productToSave.getType());
        return this.productRepository.save(newProduct);
    }
}


//    @GraphQLQuery(name = "greeting")
//    public String getGreeting(@GraphQLArgument(name = "Person", description = "Person to greet.")
//                              final Person person){
//        return "Hello "+ person.getFirstName()+"!";
