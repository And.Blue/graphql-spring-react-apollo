package com.juniorisep.deal.controller;


import com.juniorisep.deal.dtos.NewOrder;
import com.juniorisep.deal.entities.Deal;
import com.juniorisep.deal.enums.OrderStatus;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import com.juniorisep.deal.repository.DealRepository;
import io.leangen.graphql.annotations.GraphQLNonNull;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@GraphQLApi
public class DealController {
    private final DealRepository dealRepository;

    @Autowired
    public DealController(DealRepository dealRepository) {
        this.dealRepository = dealRepository;
    }


    @GraphQLQuery(name = "deals")
    public List<Deal> getAllDeals() {
        return this.dealRepository.findAll();
    }

    @GraphQLQuery(name = "deal", description = "get deal by id")
    public Deal getDealById(@GraphQLNonNull Long orderId) {
        return this.dealRepository.findById(orderId).orElseThrow(EntityNotFoundException::new);
    }

    @GraphQLQuery(name = "dealByStatus", description = "login using only an email and gives out a JWT if success")
    public List<Deal> byStatus(@GraphQLNonNull OrderStatus status) {
        return this.dealRepository.findByStatus(status);
    }

    @GraphQLQuery(name = "createOrder", description = "creates an order")
    public Deal createDeal(@GraphQLNonNull @GraphQLArgument(name = "NewOrder") NewOrder newOrder) {
        Deal newDeal = new Deal();
        //TODO : USE ModelMapper instead of get/sets
        newDeal.setClientName(newOrder.getClientName());
        newDeal.setDealerName(newOrder.getDealerName());
        newDeal.setLocation(newOrder.getLocation());

        return this.dealRepository.save(newDeal);
    }
}


//    @GraphQLQuery(name = "greeting")
//    public String getGreeting(@GraphQLArgument(name = "Person", description = "Person to greet.")
//                              final Person person){
//        return "Hello "+ person.getFirstName()+"!";
