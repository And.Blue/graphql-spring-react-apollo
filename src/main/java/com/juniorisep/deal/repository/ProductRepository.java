package com.juniorisep.deal.repository;


import com.juniorisep.deal.entities.Deal;
import com.juniorisep.deal.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAll();
}
