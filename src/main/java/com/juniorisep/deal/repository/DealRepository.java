package com.juniorisep.deal.repository;


import com.juniorisep.deal.entities.Deal;
import com.juniorisep.deal.enums.OrderStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRepository extends CrudRepository<Deal, Long> {
    List<Deal> findAll();
    List<Deal> findByStatus(OrderStatus status);
}
