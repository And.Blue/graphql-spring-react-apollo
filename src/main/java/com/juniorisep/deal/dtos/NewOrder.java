package com.juniorisep.deal.dtos;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@ToString
public class NewOrder {
    @GraphQLQuery(description = "Fullname of the client")
    private String clientName;
    @GraphQLQuery(description = "Alias name of the dealer")
    private String dealerName;
    @GraphQLQuery(description = "Location of the Deal")
    private String location;

}
