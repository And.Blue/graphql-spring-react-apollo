package com.juniorisep.deal.dtos;
import com.juniorisep.deal.enums.ProductType;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;



@Getter
@Setter
@NoArgsConstructor
@ToString
public class NewProduct {
    @GraphQLQuery(description = "Product name")
    private String name;
    @GraphQLQuery(description = "Price of a the packaged product (total weight)")
    private float price;
    @GraphQLQuery(description = "Weight/quantity of the product in grams/ml")
    private float quantity;
    @GraphQLQuery(description = "Type of the product, used to identity what it is and the weigth/quantity of it")
    private ProductType type;
    @GraphQLQuery
    private String description;
}
