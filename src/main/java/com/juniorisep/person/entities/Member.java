package com.juniorisep.person.entities;

import com.juniorisep.person.enums.Gender;
import com.juniorisep.project.entity.Project;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Table(name = "members")
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GraphQLQuery(description = "a Member's unique id")
    private Long id;
    @GraphQLQuery
    private String firstName;
    @GraphQLQuery
    private String lastName;
    @GraphQLQuery(description = "email used to login")
    private String email;

    @GraphQLQuery(description = "The member's gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToMany(mappedBy = "participants", fetch = FetchType.LAZY)
    @GraphQLQuery
    private List<Project> projects;


}
