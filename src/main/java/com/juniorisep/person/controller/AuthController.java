package com.juniorisep.person.controller;


import com.juniorisep.person.entities.Member;
import com.juniorisep.deal.repository.DealRepository;
import com.juniorisep.person.repository.MemberRepository;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLNonNull;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

@Service
@GraphQLApi
public class AuthController {
    private final MemberRepository memberRepository;

    @Autowired
    public AuthController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }



    //FOLLOW https://www.callicoder.com/spring-boot-spring-security-jwt-mysql-react-app-part-2/
    @GraphQLQuery(name = "login", description = "login using only an email and gives out a JWT if success")
    public Member login(@GraphQLNonNull String email) {
        return this.memberRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
    }


    @GraphQLMutation(name = "register", description = "Registers Creates a person by email")
    public Member register(@GraphQLNonNull String email) {
        if (emailExists(email))
            throw new EntityExistsException("User " + email + " already exists");
        Member newMember = new Member();
        newMember.setEmail(email);
        return memberRepository.save(newMember);
    }

    private boolean emailExists(String email) {
        return memberRepository.findByEmail(email).isPresent();
    }

}


//    @GraphQLQuery(name = "greeting")
//    public String getGreeting(@GraphQLArgument(name = "Person", description = "Person to greet.")
//                              final Person person){
//        return "Hello "+ person.getFirstName()+"!";
