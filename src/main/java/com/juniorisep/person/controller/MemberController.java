package com.juniorisep.person.controller;


import com.juniorisep.person.entities.Member;
import com.juniorisep.person.repository.MemberRepository;
import io.leangen.graphql.annotations.*;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@GraphQLApi
public class MemberController {
    private final MemberRepository memberRepository;

    @Autowired
    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }


    @GraphQLQuery(name = "members", description = "retrieves every member of databae")
    public List<Member> getMembers() {
        return this.memberRepository.findAll();
    }

    @GraphQLQuery(name = "memberById", description = "gets a member by his id")
    public Member getMember(long id) {
        return this.memberRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @GraphQLMutation(name = "saveMember", description = "Saves a person to DB")
    public Member saveMember(@GraphQLArgument(name = "Member", description = "Person to add") Member memberToSave) {
        return this.memberRepository.save(memberToSave);
    }
}

