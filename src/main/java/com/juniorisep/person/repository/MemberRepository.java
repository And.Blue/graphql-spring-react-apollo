package com.juniorisep.person.repository;


import com.juniorisep.person.entities.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends CrudRepository<Member, Long> {
    Optional<Member> findByEmail(String email);
    List<Member> findAll();
    List<Member> findByIdIn(List<Long> ids);
}
