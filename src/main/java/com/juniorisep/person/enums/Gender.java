package com.juniorisep.person.enums;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
