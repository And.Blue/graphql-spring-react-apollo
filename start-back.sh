#filename: start-api.sh
#!/bin/bash
# This script starts phoenix spring backend. 
# Change ./phoenix-api to the relative path to your phoenix-api directory
# On the terminal run "chmod +x ./start-api.sh" to make this script executable
./mvnw spring-boot:run -Dspring.profiles.active=dev
