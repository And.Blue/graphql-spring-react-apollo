import {gql} from "apollo-boost";


export const GET_ALL_PROJECTS= gql`
    query  GetAllProjects {
        projects {
            id
            name
            description
            createdAt
        }
    }
`;
