import React from 'react';
import {useQuery} from "@apollo/react-hooks";
import {ProjectList} from "./types";
import {GET_ALL_PROJECTS} from "./queries";
import ProjectCard from "./ProjectCard/ProjectCard";
import {createStyles, Grid, makeStyles, Theme} from "@material-ui/core";
import {CircularSpinner} from "../../../common/Spinner";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        projectCardWrapper: {
            display: 'flex',
            margin: '1rem'
        },
        projectCard: {
            margin: '1rem'
        },
        root: {
            flexGrow: 1,
        },
        paper: {
            height: 140,
            width: 100,
        },
        control: {
            padding: theme.spacing(2),
        },
    }));





export const ProjectBoard = () => {
    const classes = useStyles();
    const {loading, data, error} = useQuery<ProjectList>(GET_ALL_PROJECTS);
    return (
        <Grid container justify="center" spacing={2}>
            {error && (<p style={{color: 'red'}}>Error</p>)}
            {loading ? <CircularSpinner/> : (
                <div className={classes.projectCardWrapper}>
                    {data && data.projects.map(project => (
                        <div key={project.id} className={classes.projectCard}>
                            <ProjectCard project={project}/>
                        </div>
                    ))}
                </div>
            )
            }
        </Grid>
    );
};
