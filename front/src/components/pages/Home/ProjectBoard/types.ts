
export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
}

export interface ProjectList {
    projects: Project[];
}
