import React from "react";
import Typography from "@material-ui/core/Typography";
import withStyles, {WithStyles} from "@material-ui/core/styles/withStyles";
import {styles} from "../../../theme";
import {ProjectBoard} from "./ProjectBoard";


interface Props extends WithStyles<typeof styles> {
}

export const HomePage = withStyles(styles)(({classes}: Props) => (
    <>
        <h2>Project Board</h2>
        <ProjectBoard/>
    </>
));