import React from 'react'
import {Route, HashRouter, Switch} from 'react-router-dom';

import {Layout} from '../Layout';
import {MuiThemeProvider} from "@material-ui/core";
import {theme} from "../../../theme";
import {HomePage} from "../../pages/Home";

export const Routes: React.FC = () => (
    <MuiThemeProvider theme={theme}>
        <HashRouter>
            <Layout>
                <Switch>
                    <Route exact path='/' component={HomePage}/>
                </Switch>
            </Layout>
        </HashRouter>
    </MuiThemeProvider>

);

