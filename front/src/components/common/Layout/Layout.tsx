import React, {ReactNode, useContext} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Container, createStyles, Paper, Theme} from "@material-ui/core";
import {BottomAppBar, CustomAppBar} from "../AppBar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {IUserContext} from "../../../providers/UserProvider";


interface Props {
    children: ReactNode;
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginTop: '1rem',
            padding: theme.spacing(3, 2),
            backgroundColor: 'white',
        },
    }),
);

export const Layout = ({children}: Props) => {
        const classes = useStyles();
        return (
            <React.Fragment>
                <CssBaseline/>
                <CustomAppBar/>
                <Container maxWidth={"lg"}>
                    <Paper className={classes.root}>
                        {children}
                    </Paper>
                </Container>
                <BottomAppBar/>
            </React.Fragment>
        );

    }
;