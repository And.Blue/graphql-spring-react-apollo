import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        progress: {
            margin: theme.spacing(2),
        },
        spinnerContainer: {
            width: '100%',
            textAlign: 'center'
        }
    }),
);

export const CircularSpinner = ()  => {
    const classes = useStyles();
    return (
        <div className={classes.spinnerContainer}>
            <CircularProgress className={classes.progress}/>
        </div>
    );
};