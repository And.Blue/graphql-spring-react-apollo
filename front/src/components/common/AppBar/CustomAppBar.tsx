import {
    createStyles,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField,
    Theme,
    Toolbar
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar/AppBar";
import React, {FormEvent, FormEventHandler, useContext, useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {IUserContext, UserContext} from "../../../providers/UserProvider";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        typography: {
            padding: theme.spacing(2),
        },
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
        dialog: {
            background: 'white'
        },
        userDisplay: {
            margin: '0 1rem'
        },
    }),
);


export function CustomAppBar() {
    const userContext = useContext<IUserContext>(UserContext);

    const [open, setOpen] = React.useState(false);
    const [email, setEmail] = useState("");

    function handleClickOpen(): void {
        setOpen(true);
    }

    function handleClose(): void {
        setOpen(false);
    }

    function handleSubmit(event: FormEvent): void {
        event.preventDefault();
        console.log('submitting', email);
    }

    const classes = useStyles();

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" className={classes.title}>
                    GraphQL Demo
                </Typography>
                <div className={classes.userDisplay}>
                    <Typography variant="overline">
                        {userContext && userContext.username}
                    </Typography>
                </div>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogContent className={classes.dialog}>
                        <DialogContentText>
                            This loads context
                        </DialogContentText>
                        <form onSubmit={handleSubmit}>
                            <TextField
                                onChange={e => setEmail(e.target.value)}
                                value={email}
                                autoFocus
                                margin="dense"
                                label="Email Address"
                                type="email"
                                fullWidth
                            />
                        </form>
                    </DialogContent>
                </Dialog>
                {!userContext.email && <Button color="inherit" onClick={handleClickOpen}>Login</Button>}
                {userContext.email && <Button color="inherit" onClick={() => alert("not implemented yet")} >logout</Button>}
            </Toolbar>
        </AppBar>
    );
}