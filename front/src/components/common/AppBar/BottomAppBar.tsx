import React, {useState} from 'react';
import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import {AppBar, Modal, SwipeableDrawer} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import MoreIcon from '@material-ui/icons/MoreVert';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            padding: theme.spacing(2, 2, 0),
        },
        paper: {
            paddingBottom: 50,
        },
        list: {
            marginBottom: theme.spacing(2),
        },
        subheader: {
            backgroundColor: theme.palette.background.paper,
        },
        appBar: {
            top: 'auto',
            bottom: 0,
        },
        grow: {
            flexGrow: 1,
        },
        fabButton: {
            position: 'absolute',
            zIndex: 1,
            top: -30,
            left: 0,
            right: 0,
            margin: '0 auto',
        },
    }),
);


export const BottomAppBar = () => {
    type DrawerSide = 'top' | 'left' | 'bottom' | 'right';
    const toggleDrawer = (value: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event &&
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }

        setModalOpen(value);
    };

    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const classes = useStyles();
    return (
        <AppBar position="fixed" color="primary" className={classes.appBar}>
            <SwipeableDrawer
                anchor="left"
                open={modalOpen}
                onClose={toggleDrawer(false)}
                onOpen={toggleDrawer(true)}
            >
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae doloribus exercitationem optio
                    perferendis. Cum delectus ex facere pariatur reprehenderit, sit soluta voluptates. Accusantium
                    perferendis perspiciatis sit. Amet ex reprehenderit ullam.</p>
            </SwipeableDrawer>

            <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="open drawer">
                    <MenuIcon/>
                </IconButton>
                <Fab color="secondary" onClick={() => setModalOpen(true)} aria-label="add"
                     className={classes.fabButton}>
                    <AddIcon/>
                </Fab>
                <div className={classes.grow}/>
                <IconButton color="inherit">
                    <SearchIcon/>
                </IconButton>
                <IconButton edge="end" color="inherit">
                    <MoreIcon/>
                </IconButton>
            </Toolbar>
        </AppBar>
    );
}