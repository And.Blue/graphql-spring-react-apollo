import React from 'react';

import { ApolloProvider } from '@apollo/react-hooks';

import './App.css';
import {MuiThemeProvider as ThemeProvider} from "@material-ui/core";
import {theme} from './theme';
import {Routes} from './components/common';
import {client} from "./providers/ApolloClient";



const App: React.FC = () => {
    return (
        <ApolloProvider client={client}>
            <ThemeProvider theme={theme}>
                <Routes/>
            </ThemeProvider>
        </ApolloProvider>
    );
};
export default App;
