

interface ImageUrlWithDescription {
    src: string;
    description: string;
}


const ImageSet : ImageUrlWithDescription[] = [
    {src: "https://upload.wikimedia.org/wikipedia/commons/1/17/GraphQL_Logo.svg", description: "GraphQL Logo"},
    {src: "https://commons.wikimedia.org/wiki/Category:Angular#/media/File:Angular_full_color_logo.svg", description: "Angular Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/4/47/React.svg", description: "ReactJS Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/f/f1/Vue.png", description: "VueJS Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/3/3c/Nuxt-js.png", description: "NuxtJs Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/6/69/Polymer_Project_logo.png", description: "Polymer Project Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/2/20/Backbone.js_logo.svg", description: "Backbone.js Logo"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/2/27/Ember-logo.png", description: "EmberJSLogo"},
    {src: "https://commons.wikimedia.org/wiki/File:JQuery-Logo.svg", description: "JQuery"},
    {src: "https://upload.wikimedia.org/wikipedia/commons/a/a4/Meteor-logo.png", description: "Meteor Js"},
];

export const generateRandomImage = () : ImageUrlWithDescription =>
    ImageSet[Math.floor(Math.random() * Math.floor(ImageSet.length))];