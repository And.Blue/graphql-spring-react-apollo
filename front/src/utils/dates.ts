
export const prettyDate = (date: Date): string => new Date(date).toLocaleString();
