import {createMuiTheme, createStyles, Theme} from "@material-ui/core";
import {ThemeOptions} from "@material-ui/core/styles/createMuiTheme";

const backgroundShape = require('./assets/background.svg');

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#8A73B4",
        },
        secondary: {
            main: "#f0f0f0",
            contrastText: "#252525",
        },
        background: `url(${backgroundShape}) no-repeat`,
        flexGrow: 1,
        overflow: 'hidden',
        backgroundSize: 'cover',
        text: {
            primary: "#000000",
            secondary: "#999999",
        },
    },
    overrides: {
        MUIDataTableToolbar: {
            root: {
                backgroundColor: "#8A73B4",
                borderRadius: "4px 4px 0px 0px",
            },
            iconActive: {
                color: "black",
            },
            titleText: {
                color: "#f0f0f0",
            },
            icon: {
                color: "#f0f0f0",
                "&:hover": {
                    color: "#e0e0e0",
                },
            },
        },
        MUIDataTablePagination: {
            root: {
                boxShadow: "0px -1px #e0e0e0",
                borderBottom: "none",
            },
        },
        MUIDataTableBodyCell: {
            root: {
                cursor: "pointer",
                color: "#444444",
            },
        },
        MUIDataTableHeadCell: {
            data: {
                color: "#444444",
            },
            fixedHeader: {
                color: "#444444",
            },
        },
        MuiTableCell: {
            footer: {
                color: "#bebebe",
            },
            head: {
                color: "#8A73B4",
            },
        },
        MuiButton: {
            textPrimary: {
                color: "#8A73B4",
            },
        },
        MUIDataTableFilter: {
            resetLink: {
                marginBottom: "4px",
            },
        },
        MuiFormLabel: {
            root: {
                color: "#8A73B4",
                "&$focused": {
                    // increase the specificity for the pseudo class
                    color: "#8A73B4",
                },
            },
        },
        MuiListItem: {
            root: {
                "&$selected": {
                    // increase the specificity for the pseudo class
                    color: "#8A73B4",
                    backgroundColor: "#e3daf1",
                    "&:hover": {
                        backgroundColor: "#e3daf1",
                    },
                },
                "&:hover": {
                    color: "#8A73B4",
                    backgroundColor: "#f8f6ff",
                },
            },
            button: {
                "&:hover": {
                    backgroundColor: "#f8f6ff",
                },
            },
        },
        MuiInput: {
            underline: {
                "&::after": {
                    borderBottom: "2px solid #8A73B4",
                },
            },
        },
        MUIDataTableSearch: {
            clearIcon: {
                "&:hover": {
                    color: "black",
                },
            },
        },
        MUIDataTableSelectCell: {
            checkboxRoot: {
                "&$checked": {
                    // increase the specificity for the pseudo class
                    color: "#8A73B4",
                },
            },
        },
        MuiTypography: {
            colorTextSecondary: {
                color: "rgba(0,0,0,0.54)",
            },
        },
        MuiInputBase: {
            root: {
                lineHeight: "revert",
            },
        },
        MuiCheckbox: {
            colorSecondary: {
                "&$checked": {
                    // increase the specificity for the pseudo class
                    color: "#8A73B4",
                },
            },
        },
        MuiSelect: {
            select: {
                "&:focus": {
                    borderRadius: "3px",
                    backgroundColor: "rgba(237, 231, 246,0.5)",
                    padding: "5px",
                    margin: "2px",
                },
                "&:hover": {
                    borderRadius: "3px",
                    backgroundColor: "rgba(237, 231, 246,0.5)",
                    padding: "5px",
                    margin: "2px",
                },
            },
        },
    },
} as ThemeOptions);



export const styles = (theme: Theme) => createStyles({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.grey['100'],
        overflow: 'hidden',
        background: `url(${backgroundShape}) no-repeat`,
        backgroundSize: 'cover',
        paddingBottom: 1000,
    },
    grid: {
        width: 1200,
        marginTop: 40,
        [theme.breakpoints.down('sm')]: {
            width: 'calc(100% - 20px)'
        }
    },
    paper: {
        padding: theme.spacing(3),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    rangeLabel: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingTop: theme.spacing(2)
    },
    topBar: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 32
    },
    outlinedButtom: {
        textTransform: 'uppercase',
        margin: theme.spacing(1)
    },
    actionButtom: {
        textTransform: 'uppercase',
        margin: theme.spacing(1),
        width: 152
    },
    blockCenter: {
        padding: theme.spacing(2),
        textAlign: 'center'
    },
    block: {
        padding: theme.spacing(2),
    },
    box: {
        marginBottom: 40,
        height: 65
    },
    inlining: {
        display: 'inline-block',
        marginRight: 10
    },
    buttonBar: {
        display: 'flex'
    },
    alignRight: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    noBorder: {
        borderBottomStyle: 'hidden'
    },
    loadingState: {
        opacity: 0.05
    },
    loadingMessage: {
        position: 'absolute',
        top: '40%',
        left: '40%'
    }
});