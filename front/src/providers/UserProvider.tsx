import React, {Context, createContext, ProviderProps, ReactNode, Reducer, useContext, useReducer} from 'react';

export interface IUserContext {
    username: string;
    email: string;

}

export interface ProviderProps {
    children: ReactNode;
    reducer: Reducer<IUserContext, Action>;
    initialState: IUserContext;
}

export type Action = {type: 'LOGIN'} | {type: 'LOGOUT'};

export let initialState: IUserContext = {
    username: "apouret",
    email: "apouret@juniorisep.com"
};

export const reducer: Reducer<IUserContext, Action> = (state, action) => {
    switch (action.type) {
        case 'LOGIN' :
            return {
                ...state,
                email: "andrew@dsd"
            };
        case 'LOGOUT':
            return {
                ...state,
                email: ""} as IUserContext;
        default:
            return state;
    }
};

//https://medium.com/front-end-weekly/react-hooks-tutorial-for-pure-usereducer-usecontext-for-global-state-like-redux-and-comparison-dd3da5053624
//https://hackernoon.com/state-management-with-react-context-typescript-and-graphql-fb6264314a15
export const UserContext: Context<IUserContext> = createContext(initialState);
//
// export const UserProvider = ({children}: ProviderProps<IUserContext>) => (
//     <UserContext.Provider value={UserContext}>{children}</UserContext.Provider>
// );


