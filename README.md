
## Graphql Spring React Apollo : PrePhoenix 

This App is meant to test out a spring implementation of java-graphql and explain in a *simple POC Management App* how we can adapt Phoenix's current RESTFUL Api into a GraphQL Api, and adapt a React redux SPA (Single Page Application) into a React Ts Appollo App using the natif React Context.  

#### Dev : Getting started!

#### Backend
- Using IntelliJ, add Maven Plugin to automatically handle dependencies.  
- For the database, you must run an instance of MySQL (WAMP or DOCKER...etc.) and change the current mysql connection string at `spring.datasource.url` in _applications.properties_ (within _resources_ in _src_).
- (optional) run `sample-db-insert.sql` into the database.
- Run Project !!!
- Open up your browser on `localhost:8000/gui` and have fun on GraphiQL (a sort of GraphQL Postman with the documentation of the Schema). 

ps: _if your prefer using Postman instead of GraphiQl (its easier to handle headers and save queries/mutations), add the collection in this repo and head over to this to set up the Schema in Postman_ [link](https://learning.getpostman.com/docs/postman/sending_api_requests/graphql/#importing-graphql-schemasif])


